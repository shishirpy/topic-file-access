# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 20:09:42 2014

@author: shishir

Read pdf files and extract text from them. The code should be able to figure
out if the file has no text. 


The program used the pdfminer library. The main code is borrowed from 
stackoverflow:
http://bit.ly/1urnD17
"""

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO

import os
import re

def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = file(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
        interpreter.process_page(page)
    fp.close()
    device.close()
    str = retstr.getvalue()
    retstr.close()
    return str


if __name__ == "__main__":
    src_path = "../../datasets/topic-file/pdfs/"
    dst_path = "../../datasets/topic-file/texts/"
    
    for root, subfolders, files in os.walk(src_path):
        for f in files:
            m = re.findall('\.pdf$', f)
            if m:
                try:
                    text = convert_pdf_to_txt(root + '/' + f)
                except:
                    print "ERROR"
                    
                    
            